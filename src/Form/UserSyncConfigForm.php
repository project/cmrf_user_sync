<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Form;

use Drupal\cmrf_user_sync\Plugin\UserSyncManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UserSyncConfigForm.
 */
class UserSyncConfigForm extends ConfigFormBase {

  protected $core;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * @var \Drupal\cmrf_user_sync\Plugin\UserSyncManager
   */
  protected $userSyncManager;

  public function __construct($core, $queue, UserSyncManager $userSyncManager) {
    $this->core = $core;
    $this->queue = $queue;
    $this->userSyncManager = $userSyncManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cmrf_core.core'),
      $container->get('queue')->get('cmrf_user_sync_queue', TRUE),
      $container->get('plugin.manager.cmrf.user_sync')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cmrf_user_sync.usersyncconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cmrf_user_sync_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('cmrf_user_sync.usersyncconfig');
    $connection = $values['connection'] ?? $config->get('connection');
    $messagedef = $values['messagedef'] ?? $config->get('messagedef');
    $userMessageProcessorPlugin = $values['user_message_processor_plugin'] ?? $config->get('user_message_processor_plugin');

    $form['usersync'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('User Sync Configuration'),
      '#attributes' => ['id' => 'user-sync-stuff'],
    ];
    $form['usersync']['connection'] = [
      '#type' => 'select',
      '#title' => $this->t('Connection'),
      '#description' => $this->t('Select the connection'),
      '#options' => ['0' => t('-Select-')] + $this->core->getConnectors(),
      '#default_value' => $config->get('connection') ?: 0,
      '#ajax' => [
        'callback' => [$this, 'connectionCallback'],
        'wrapper' => 'user-sync-stuff',
        'event'   => 'change',
      ],
    ];
    if ($connection) {
      $form['usersync']['messagedef'] = [
        '#type' => 'select',
        '#title' => $this->t('Change MessageDefinition'),
        '#options' => ['0' => t('-Select-')] + $this->getMessageDefinitions($connection),
        '#default_value' => $config->get('messagedef') ?: 0,
        '#ajax' => [
          'callback' => [$this, 'connectionCallback'],
          'wrapper' => 'user-sync-stuff',
          'event'   => 'change',
        ],
      ];
      $form['usersync']['is_active'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Enabled'),
        '#default_value' => $config->get('is_active'),
        '#description' => t('Is the user sync active (the cron is used for the updates)'),
      ];
    }

    $processors = [];
    foreach ($this->userSyncManager->getDefinitions() as $definition) {
      $processors[$definition['id']] = $definition['label'];
    }

    $form['usersyncprocessing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Processing'),
      '#attributes' => ['id' => 'user-sync-processor'],
    ];

    $form['usersyncprocessing']['user_message_processor_plugin'] = [
      '#type' => 'select',
      '#title' => 'Processing',
      '#default_value' => $userMessageProcessorPlugin,
      '#options' => $processors,
      '#ajax' => [
        'callback' => [$this, 'processorCallback'],
        'wrapper' => 'user-sync-processor',
        'event'   => 'change',
      ],
    ];

    if ($userMessageProcessorPlugin) {
      $userSyncPlugin = $this->userSyncManager->createInstance($userMessageProcessorPlugin);
      if ($userSyncPlugin) {
        $form = $userSyncPlugin->buildForm($form, $form_state, $config, $connection, $messagedef);
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function connectionCallback(array $form, FormStateInterface $form_state) {
    return $form['usersync'];
  }

  /**
   *
   */
  public function processorCallback(array $form, FormStateInterface $form_state) {
    return $form['usersyncprocessing'];
  }

  /**
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('cmrf_user_sync.usersyncconfig');
    $connection = $values['connection'] ?? $config->get('connection');
    $messagedef = $values['messagedef'] ?? $config->get('messagedef');

    if ($values['user_message_processor_plugin']) {
      $userSyncPlugin = $this->userSyncManager->createInstance($values['user_message_processor_plugin']);
      if ($userSyncPlugin) {
        $userSyncPlugin->validateForm($form, $form_state, $config, $connection, $messagedef);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('cmrf_user_sync.usersyncconfig');
    $config->set('connection', $form_state->getValue('connection'));
    $config->set('messagedef', $form_state->getValue('messagedef'));
    $config->set('is_active', $form_state->getValue('is_active'));
    $config->set('user_message_processor_plugin', $form_state->getValue('user_message_processor_plugin'));

    if ($config->get('user_message_processor_plugin')) {
      $userSyncPlugin = $this->userSyncManager->createInstance($config->get('user_message_processor_plugin'));
      if ($userSyncPlugin) {
        $userSyncPlugin->submitForm($form, $form_state, $config, $config->get('connection'), $config->get('messagedef'));
      }
    }

    $config->save();
    if ($form_state->getValue('is_active')) {
      // Do nothing.
    }
    else {
      $this->queue->deleteQueue();
    }
  }

  /**
   *
   */
  private function getMessageDefinitions($connection) {
    $call = $this->core->createCall($connection, 'ChangeMessageDefinition', 'get', [], ['limit' => 0]);
    $result = $this->core->executeCall($call);
    $md = [];
    foreach ($result['values'] as $value) {
      $md[$value['name']] = $value['title'];
    }
    return $md;
  }

}
