<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin\views\argument_default;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\views\Plugin\views\argument_default\ArgumentDefaultPluginBase;

/**
 * Default argument plugin to extract the current user.
 *
 * This plugin actually has no options so it does not need to do a great deal.
 *
 * @ViewsArgumentDefault(
 *   id = "cmrf_user_contact_id",
 *   title = @Translation("CiviCRM Contact ID from logged in user")
 * )
 */
class CurrentUserContactId extends ArgumentDefaultPluginBase implements CacheableDependencyInterface {

  /**
   * {@inheritdoc}
   */
  public function getArgument() {
    $id = \Drupal::currentUser()->id();
    $currentUser = \Drupal::entityTypeManager()->getStorage('user')->load($id);
    return $currentUser->field_user_contact_id->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return ['user'];
  }

}
