<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin\QueueWorker;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Plugin implementation of the cmrf_user_sync_queue queueworker.
 *
 * @QueueWorker (
 *   id = "cmrf_user_sync_queue",
 *   title = @Translation("Synchronize users from CiviCRM Change Messages"),
 *   cron = {"time" = 30}
 * )
 */
class UserSyncQueue extends QueueWorkerBase {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  public $config;

  /**
   * @var \Drupal\Core\Queue\QueueInterface
   */
  public $queue;

  /**
   * @var \CMRF\Core\Core|mixed
   */
  public $core;

  /**
   * @var \Drupal\cmrf_user_sync\Plugin\UserSyncManager|mixed
   */
  public $userSyncManager;

  /**
   * Constructs a \Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = \Drupal::config('cmrf_user_sync.usersyncconfig');
    $this->queue = \Drupal::service('queue')->get('cmrf_user_sync_queue');
    $this->core = \Drupal::service('cmrf_core.core');
    $this->userSyncManager = \Drupal::service('plugin.manager.cmrf.user_sync');
  }

  /**
   *
   */
  private function actionGet(): void {
    $connection = $this->config->get('connection');
    $messagedef = $this->config->get('messagedef');

    $call = $this->core->createCall($connection, 'ChangeMessageApiQueue', 'get', ['name' => $messagedef]);
    $result = $this->core->executeCall($call);
    if ($result['is_error']) {
      \Drupal::logger('cmrf_user_sync')->error('Error using connection @connection with messsage definition @messagedef ' . $result['message'], [
        '@connection' => $connection,
        '@messagedef' => $messagedef,
      ]);
      return;
    }
    foreach ($result['values'] as $value) {
      $this->queue->createItem(['process', $value['message']]);
    }
  }

  /**
   * @param array $message
   */
  private function actionProcess(array $message): void {
    $create_user_plugin = $this->config->get('user_message_processor_plugin');
    try {
      $crt = $this->userSyncManager->createInstance($create_user_plugin);
      $contactId = 0;
      if ($this->config->get('contact_id') && isset($message[$this->config->get('contact_id')])) {
        $contactId = $message[$this->config->get('contact_id')] ?? 0;
      }
      $crt->process($contactId, $message, $this->config);
    }
    catch (PluginException $e) {

    }
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($item): void {
    try {
      [$action, $data] = $item;
      switch ($action) {
        case 'get':
          $this->actionGet();
          break;

        case 'process':
          $this->actionProcess($data);
          break;

        default:
      }
    }
    catch (\Exception $ex) {
      \Drupal::logger('cmrf_user_sync')
        ->error('Exception in @ex on processing @item ', [
          '@ex' => $ex->getMessage(),
          '@item' => $item,
        ]);
    }
  }

}
