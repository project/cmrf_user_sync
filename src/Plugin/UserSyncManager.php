<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the User create plugin manager.
 */
class UserSyncManager extends DefaultPluginManager {

  /**
   * Constructs a new UserCreateManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/UserMessageProcessor', $namespaces, $module_handler, 'Drupal\cmrf_user_sync\Plugin\UserMessageProcessorInterface', 'Drupal\cmrf_user_sync\Annotation\UserMessageProcessor');

    $this->alterInfo('cmrf_user_sync_user_sync_info');
    $this->setCacheBackend($cache_backend, 'cmrf_user_sync_user_message_processor_plugins');
  }

}
