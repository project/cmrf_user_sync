<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for User create plugins.
 */
abstract class UserMessageProcessorBase extends PluginBase implements ContainerFactoryPluginInterface, UserMessageProcessorInterface {

  protected $core;

  /**
   * The queue object.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * @var \Drupal\cmrf_user_sync\Plugin\UserSyncManager
   */
  protected $userSyncManager;

  public function __construct($core, UserSyncManager $userSyncManager, array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->core = $core;
    $this->userSyncManager = $userSyncManager;
  }

  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   *   Returns an instance of this plugin.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('cmrf_core.core'),
      $container->get('plugin.manager.cmrf.user_sync'),
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Process a message.
   *
   * @param int $contact_id
   * @param array $message
   * @param \Drupal\Core\Config\Config $config
   */
  abstract public function process(int $contact_id, array $message, Config $config);

  /**
   * Returns the form.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL) {
    return $form;
  }

  /**
   * Validates the form and sets errors if there are any.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function validateForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL) {

  }

  /**
   * Process the submitted configuration.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function submitForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL) {

  }

  /**
   * Returns a list of fields available in the message definition.
   *
   * @param $connection
   * @param $messageDefinitionName
   *
   * @return array
   */
  protected function getFieldsFromMessageDefinition($connection, $messageDefinitionName) {
    $call = $this->core->createCall($connection, 'ChangeMessageDefinition', 'getsingle', ['name' => $messageDefinitionName], []);
    $result = $this->core->executeCall($call);
    $fields = $result['message_provider_fields'];
    if (!is_array($fields)) {
      $fields = [];
    }
    return $fields;

  }

}
