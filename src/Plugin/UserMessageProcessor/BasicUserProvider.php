<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin\UserMessageProcessor;

use Drupal\cmrf_user_sync\Plugin\UserMessageProcessorBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\User;

/**
 * @UserMessageProcessor (
 *   id = "cmrf_basic_user_sync",
 *   label = @Translation("Basic User Sync"),
 * )
 **/
class BasicUserProvider extends UserMessageProcessorBase {
  use StringTranslationTrait;

  /**
   * Returns the form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL) {
    $fields = $this->getFieldsFromMessageDefinition($connection, $messageDefinitionName);

    $name = $values['name'] ?? $config->get('name');
    $email = $values['email'] ?? $config->get('email');
    $contact_id = $values['contact_id'] ?? $config->get('contact_id');
    $enable_logging = $values['enable_logging'] ?? $config->get('enable_logging');

    $form['usersyncprocessing']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('This processor does as follows: <ul><li>It will create a user when the user with the contact id does not exists.</li><li>It will update the user when a user with contact id exists.</li><li>It will block the user when the email address is empty.</li></ul>'),
    ];
    $form['usersyncprocessing']['name'] = [
      '#type' => 'select',
      '#title' => $this->t('Name'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $name,
    ];
    $form['usersyncprocessing']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $email,
    ];
    $form['usersyncprocessing']['contact_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Contact ID'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $contact_id,
    ];
    $form['usersyncprocessing']['enable_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#default_value' => $enable_logging,
      '#description' => t('Is the user sync active (the cron is used for the updates)'),
    ];
    return $form;
  }

  /**
   * Validates the form and sets errors if there are any.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function validateForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL) {
    $values = $form_state->getValues();
    if ((!$values['name']&&!$values['email']) &&$values['is_active']) {
      $form_state->set('is_active', FALSE);
      $form_state->setErrorByName('is_active', $this->t('Cannot enable if contact_id, name or email is not set'));
    }
  }

  /**
   * Process the submitted configuration.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function submitForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL) {
    $config->set('name', $form_state->getValue('name'));
    $config->set('email', $form_state->getValue('email'));
    $config->set('contact_id', $form_state->getValue('contact_id'));
    $config->set('enable_logging', $form_state->getValue('enable_logging'));
  }

  /**
   * Process a message.
   *
   * @param $contact_id
   * @param $message
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   *
   * @return void
   */
  public function process($contact_id, $message, $config) {
    if ($config->get('enable_logging')) {
      \Drupal::logger('cmrf_user_sync')->notice('Processing message for Contact [@contact_id]. Message: @message', [
        '@contact_id' => $contact_id,
        '@message' => json_encode($message, JSON_PRETTY_PRINT),
      ]);
    }

    /** @var \Drupal\Core\Entity\Query\Sql\Query $query */
    $query = \Drupal::entityQuery('user');

    $users = $query->condition('field_user_contact_id', $contact_id)->execute();
    $email = $message[$config->get('email')];
    $name = $message[$config->get('name')];
    $emailExists = $this->checkIfEmailExist($email);
    if (empty($users) && !$emailExists && $email) {
      $user = User::create([
        'mail' => $email,
        'field_user_contact_id' => $contact_id,
        'name' => $name,
        'status' => 1,
      ]);
      $user->save();
      \Drupal::logger('cmrf_user_sync')->notice('Created new user @name identified with @contact_id', [
        '@name' => $name,
        '@contact_id' => $contact_id,
      ]);
    }
    elseif (empty($users) && $emailExists) {
      \Drupal::logger('cmrf_user_sync')->notice('Unable to create user @name identified with @contact_id because another user with email @email already exists', [
        '@name' => $name,
        '@contact_id' => $contact_id,
        '@email' => $email,
      ]);
    }
    elseif (count($users) == 1 && $email) {
      $user = User::load(reset($users));
      if ($emailExists) {
        \Drupal::logger('cmrf_user_sync')->notice('Could not update email for user @name identified with @contact_id because other user with email @email already exists', [
          '@name' => $name,
          '@contact_id' => $contact_id,
          '@email' => $email,
        ]);
      }
      else {
        $user->mail->value = $email;
      }
      $user->field_user_contact_id->value = $contact_id;
      $user->set('name', $name);
      $user->set('status', 1);
      $user->save();
      \Drupal::logger('cmrf_user_sync')->notice('Updates existing user @name identified with @contact_id', [
        '@name' => $name,
        '@contact_id' => $contact_id,
      ]);
    }
    elseif (count($users) == 1 && !$email) {
      $user = User::load(reset($users));
      if (!$user->hasRole('administrator')) {
        $user->set('status', 0);
        $user->save();
      }
    }
  }

  /**
   * Check if an user with a given email already exists.
   *
   * @param $email
   *
   * @return bool
   */
  private function checkIfEmailExist($email) {
    /** @var \Drupal\Core\Entity\Query\Sql\Query $query */
    $query = \Drupal::entityQuery('user');
    $query->accessCheck(FALSE);
    $users = $query->condition('mail', $email)->execute();
    return count($users) > 0;
  }

}
