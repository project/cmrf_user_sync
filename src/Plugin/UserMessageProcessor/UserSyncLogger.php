<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin\UserMessageProcessor;

use Drupal\cmrf_user_sync\Plugin\UserMessageProcessorBase;

/**
 * @UserMessageProcessor (
 *   id = "cmrf_user_sync_logger",
 *   label = @Translation("Log the user message"),
 * )
 **/
class UserSyncLogger extends UserMessageProcessorBase {

  /**
   * Process a message.
   *
   * @param $contact_id
   * @param $message
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   *
   * @return void
   */
  public function process($contact_id, $message, $config) {
    \Drupal::logger('cmrf_user_sync')->notice('Processing message for Contact [@contact_id]. Message: @message', [
      '@contact_id' => $contact_id,
      '@message' => json_encode($message, JSON_PRETTY_PRINT),
    ]);
  }

}
