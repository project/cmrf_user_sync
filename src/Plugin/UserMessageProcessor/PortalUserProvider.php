<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin\UserMessageProcessor;

use Drupal\cmrf_user_sync\Plugin\UserMessageProcessorBase;
use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * @UserMessageProcessor (
 *   id = "cmrf_portal_user_sync",
 *   label = @Translation("Portal User Sync"),
 * )
 **/
class PortalUserProvider extends UserMessageProcessorBase {
  use StringTranslationTrait;

  /**
   * Returns the form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL): array {
    $fields = $this->getFieldsFromMessageDefinition($connection, $messageDefinitionName);

    $name = $values['name'] ?? $config->get('name');
    $email = $values['email'] ?? $config->get('email');
    $old_email = $values['email'] ?? $config->get('old_email');
    $roles = $values['roles'] ?? $config->get('roles');

    $form['usersyncprocessing']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Use this processor when you have the portal user extension in CiviCRM. <br>It does the following : <ul><li>It will create a user when the user with the e-mail address does not exists.</li><li>It will update the user when a user with e-mail address exists.</li><li>It will delete the user when the email address is empty.</li></ul>'),
    ];
    $form['usersyncprocessing']['name'] = [
      '#type' => 'select',
      '#title' => $this->t('Username'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $name,
    ];
    $form['usersyncprocessing']['email'] = [
      '#type' => 'select',
      '#title' => $this->t('Email'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $email,
      '#required' => TRUE,
    ];
    $form['usersyncprocessing']['old_email'] = [
      '#type' => 'select',
      '#title' => $this->t('Old Email'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $old_email,
      '#required' => TRUE,
    ];
    $form['usersyncprocessing']['roles'] = [
      '#type' => 'select',
      '#title' => $this->t('Roles'),
      '#options' => ['0' => t('-Select-')] + $fields,
      '#default_value' => $roles,
    ];

    foreach ($this->getAvailableFields() as $fieldName => $field) {
      $selectFieldValue = $values[$fieldName] ?? $config->get($fieldName);
      $form['usersyncprocessing'][$fieldName] = [
        '#type' => 'select',
        '#title' => $field->getLabel(),
        '#description' => $field->getDescription(),
        '#options' => ['0' => t('-Select-')] + $fields,
        '#default_value' => $selectFieldValue,
      ];
    }

    return $form;
  }

  /**
   *
   */
  protected function getAvailableFields(): array {
    /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager */
    $entity_field_manager = \Drupal::getContainer()->get('entity_field.manager');
    $availableFields = $entity_field_manager->getFieldDefinitions('user', 'user');
    $fieldsToSkip = ['created', 'changed', 'access', 'login', 'init', 'status', 'name', 'mail'];
    foreach ($availableFields as $fieldName => $field) {
      if ($field->isReadOnly() || isset($form['usersyncprocessing'][$fieldName]) || in_array($fieldName, $fieldsToSkip)) {
        unset($availableFields[$fieldName]);
      }
    }
    return $availableFields;
  }

  /**
   * Validates the form and sets errors if there are any.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function validateForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL): void {
    $values = $form_state->getValues();
    if ((!$values['name']&&!$values['email']) &&$values['is_active']) {
      $form_state->set('is_active', FALSE);
      $form_state->setErrorByName('is_active', $this->t('Cannot enable if contact_id, name or email is not set'));
    }
  }

  /**
   * Process the submitted configuration.
   *
   * Child classes could override this function to change the configuration form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function submitForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL): void {
    $config->set('name', $form_state->getValue('name'));
    $config->set('email', $form_state->getValue('email'));
    $config->set('old_email', $form_state->getValue('old_email'));
    $config->set('contact_id', $form_state->getValue('contact_id'));
    $config->set('roles', $form_state->getValue('roles'));
    foreach ($this->getAvailableFields() as $fieldName => $field) {
      $config->set($fieldName, $form_state->getValue($fieldName));
    }
  }

  /**
   * Process a message.
   *
   * @param int $contact_id
   * @param array $message
   * @param \Drupal\Core\Config\Config $config
   */
  public function process(int $contact_id, array $message, Config $config) {
    $email = $message[$config->get('email')];

    /** @var \Drupal\Core\Entity\Query\Sql\Query $query */
    $users = \Drupal::entityQuery('user')
      ->accessCheck(FALSE)
      ->condition('mail', $email)
      ->execute();
    if (!empty($message[$config->get('old_email')])) {
      $currentUsers = \Drupal::entityQuery('user')
        ->accessCheck(FALSE)
        ->condition('mail', $message[$config->get('old_email')], 'IN')
        ->execute();
      if (count($currentUsers) && count($users)) {
        $this->deleteUsers($currentUsers, $config);
      }
      else {
        $users = $currentUsers;
      }
    }

    if (!count($users)) {
      if (!empty($email)) {
        $name = $email;
        if ($config->get('name') && !empty($message[$config->get('name')])) {
          $name = $message[$config->get('name')];
        }

        $user = User::create([
          'mail' => $email,
          'name' => $name,
          'status' => 1,
        ]);
        try {
          $user->save();
          _user_mail_notify('status_activated', $user);
          $users[] = $user->id();
        }
        catch (EntityStorageException $e) {
          \Drupal::logger('cmrf_user_sync')->error('Could not create new user because: @reason', ['@reason' => $e->getMessage()]);
        }
      }
    }

    if (empty($email)) {
      $this->deleteUsers($users, $config);
    }
    else {
      foreach ($users as $uid) {
        $this->setUserDataAndRoles($contact_id, $uid, $message, $config);
      }
    }
  }

  /**
   *
   */
  private function deleteUsers(array $users, Config $config) {
    foreach ($users as $uid) {
      $user = User::load($uid);
      $isAdmin = FALSE;
      foreach ($user->getRoles(TRUE) as $rid) {
        $role = Role::load($rid);
        if ($role && $role->isAdmin()) {
          $isAdmin = TRUE;
        }
        elseif (!empty($config->get('roles'))) {
          $user->removeRole($rid);
        }
      }
      if (!$isAdmin) {
        try {
          $user->delete();
        }
        catch (EntityStorageException $e) {
          \Drupal::logger('cmrf_user_sync')
            ->error('Could not delete user because: @reason', [
              '@reason' => $e->getMessage(),
            ]);
        }
      }
      elseif (!empty($config->get('roles'))) {
        try {
          $user->save();
        }
        catch (EntityStorageException $e) {
          \Drupal::logger('cmrf_user_sync')
            ->error('Could not roles from user because: @reason', [
              '@reason' => $e->getMessage(),
            ]);
        }
      }
    }
  }

  /**
   *
   */
  private function setUserDataAndRoles(int $contactId, int $uid, array $message, Config $config) {
    $user = User::load($uid);
    $roles = $this->getRoles($message, $config);
    if ($contactId) {
      $user->set('field_user_contact_id', $contactId);
    }
    if ($config->get('email') && !empty($message[$config->get('email')])) {
      $user->set('mail', $message[$config->get('email')]);
    }
    if ($config->get('name') && !empty($message[$config->get('name')])) {
      $user->set('name', $message[$config->get('name')]);
    }
    $user->set('status', 1);
    if (!empty($config->get('roles'))) {
      foreach ($user->getRoles(TRUE) as $rid) {
        $role = Role::load($rid);
        if (!$role || !$role->isAdmin()) {
          $user->removeRole($rid);
        }
      }
      foreach ($roles as $rid) {
        $user->addRole($rid);
      }
    }

    foreach ($this->getAvailableFields() as $fieldName => $field) {
      if ($fieldName == 'roles') {
        continue;
      }
      if ($config->get($fieldName) && isset($message[$config->get($fieldName)])) {
        $user->set($fieldName, $message[$config->get($fieldName)]);
      }
    }
    try {
      $user->save();
    }
    catch (EntityStorageException $e) {
      \Drupal::logger('cmrf_user_sync')->error('Could not update user because: @reason', ['@reason' => $e->getMessage()]);
    }
  }

  /**
   * @param array $message
   * @param \Drupal\Core\Config\Config $config
   *
   * @return array
   */
  private function getRoles(array $message, Config $config): array {
    $roles = [];
    if (empty($config->get('roles'))) {
      return $roles;
    }
    if (!isset($message[$config->get('roles')]) || !is_array($message[$config->get('roles')])) {
      return $roles;
    }

    $drupalRoles = Role::loadMultiple();
    foreach ($message[$config->get('roles')] as $role_name) {
      $isFound = FALSE;
      foreach ($drupalRoles as $drupalRole) {
        $role_id = preg_replace('@[^a-z0-9_]+@', '_', mb_strtolower($role_name));
        if ($drupalRole->label() == $role_name || $drupalRole->id() == $role_id) {
          $roles[] = $drupalRole->id();
          $isFound = TRUE;
          break;
        }
      }
      if (!$isFound) {
        $role_id = preg_replace('@[^a-z0-9_]+@', '_', mb_strtolower($role_name));
        $newRole = Role::create(['label' => $role_name, 'id' => $role_id]);
        try {
          $newRole->save();
          $roles[] = $newRole->id();
          $drupalRoles[] = $newRole;
        }
        catch (EntityStorageException $e) {
        }
      }
    }
    return $roles;
  }

}
