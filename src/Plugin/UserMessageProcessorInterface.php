<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an interface for User create plugins.
 */
interface UserMessageProcessorInterface extends PluginInspectionInterface {

  /**
   * Process a message.
   *
   * @param int $contact_id
   * @param array $message
   * @param \Drupal\Core\Config\Config $config
   */
  public function process(int $contact_id, array $message, Config $config);

  /**
   * Returns the form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return array
   */
  public function buildForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL);

  /**
   * Validates the form and sets errors if there are any.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function validateForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL);

  /**
   * Process the submitted configuration.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig $config
   * @param string|null $connection
   * @param string|null $messageDefinitionName
   *
   * @return void
   */
  public function submitForm(array $form, FormStateInterface $form_state, $config, $connection = NULL, $messageDefinitionName = NULL);

}
