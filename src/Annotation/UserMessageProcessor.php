<?php

/*
 * @author Klaas Eikelboom  <klaas.eikelboom@civicoop.org>
 * @date 17-Jan-2022
 * @license  GPL-2.0-or-later
 */

namespace Drupal\cmrf_user_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a User create item annotation object.
 *
 * @see \Drupal\cmrf_user_sync\Plugin\UserSyncManager
 * @see plugin_api
 *
 * @Annotation
 */
class UserMessageProcessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
